# BASH COURSE #
## Everything can be a script ##
### What is this repository for? ###
* This is the repository with slides for the Bash course
* Continuously updating

### How do I get set up? ###

* The repo have the last version in PDF
* If you wish, you can compile the slides with latex
* In the folder _"Examples"_ have the files for practicing the differents commands 


### Contribution guidelines ###

* If you have any question or suggestion, write to me on luis.alfredo.nu@gmail.com

